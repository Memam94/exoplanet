# Exoplanet



## Getting started

This is an analysis to search for exoplanet around a star (XO-2) using the transit method.


```
cd existing_repo
git remote add origin https://gitlab.com/Memam94/exoplanet.git
git branch -M main
git push -uf origin main
``` 

## Basic Dependance

- ```numpy```  ```matplotlib```   ```scipy```   ```random```


## Special packages
- Lightkurve ```pip install lightkurve```
- Astropy ```pip install astropy```
- Wotan ```pip install wotan```
- BATMAN ```pip install batman-package```
- Transit Least Square (LST) ```pip install transitleastsquares```
